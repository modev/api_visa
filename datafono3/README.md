# API Visa Barra libre

API Visa Barra libre es el que gestiona la conexiòn de la app de Visa Barra libre con el datafono para efectuar las compras con este.

## Conexion fisica del datafono

1- Conectar a la corriente el datafono y la pc.

2- Conectar a la red por LAN el datafono en el puerto ETH.

3- Conectar el datafono con la pc y buscar en "Administrador de Dispositivos" > "Puertos (COM y LPT)" el puerto en el que se conecto el datafono para luego configurarlo en el archivo config.sip

4- Buscar el ip de la pc por CMD con el comando: 
``` bash
ipconfig
```
5- Editar el ip de la pc en la app y volver a compilar para que se conecte sin problema al API. 

## Pre-Instalacion

1- Clonar el repositorio en la ruta que desee

2- Dar permisos de escritura y lectura a toda la carpeta 

3- Configurar el archivo config.sip

## Instalacion

Use el administrador de paquetes [npm](https://www.npmjs.com/get-npm) para instalar API Visa Barra Libre y sus dependencias.

```bash
npm install 
```

## Requerimientos

Instalar las dependencias de [pm2](http://pm2.keymetrics.io/) con los siguientes comandos:

```bash
npm install pm2 -g
```

## Iniciar API Visa Barra libre
Use el comando de [pm2](http://pm2.keymetrics.io/) para iniciar el Avanzado, administrador de procesos de producción para Node.js.

```bash
pm2 start main.js
```

## Contribucion
Las solicitudes de extracción son bienvenidas. Para cambios importantes, primero abra un problema para discutir qué le gustaría cambiar.

Por favor, asegúrese de actualizar las pruebas según corresponda.

## Licencia
[MIT](https://choosealicense.com/licenses/mit/)