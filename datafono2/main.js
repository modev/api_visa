const fs = require('fs'); // to create, edit and read file
const nodemailer = require("nodemailer"); // to create and send email in node.

var bodyParser = require('body-parser')
var express = require('express');
var app = express();

app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(express.json());       // to support JSON-encoded bodies
app.use((req, res, next) => {  // to allow all request to any computer
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});
/**
 * Esta funcion contiene todo el proceso desde que se recibe la peticion hasta que se obtiene la respuesta del datafono
*/
app.post('/create', function(req, res) {

    // Se obtiene lo que envia la papp
    var request = req.body
    console.log(req.body)
    var monto = request.monto
    var iva = 0
    var id_factura = request.id_factura 
    var base_dev = request.base_dev 
    var imp_consumo = 0
    var cajero = request.cajero
    var user_email = request.email

    var compra = "0,"+monto+","+iva+","+id_factura+","+base_dev+","+imp_consumo+","+cajero
    var filepath = "SOLICITUD.txt";
    
    // Se escribe al archivo para hacer la solicitud al datafono
    fs.writeFile(filepath, compra, function(err) {
        if(err) {
            return console.log(err);
        }
        console.log("The file was saved!");
     });

    // Se ejecuta el .exe que se conectara con el datafono
    var exec = require('child_process').execFile;
    var abrirProgramaDatafono = function(){
    console.log("fun() start");
    var prog = exec('Cajas.exe', function(err, data) {  
            console.log(err)
            console.log(data.toString());                       
        });  
    return  prog;
    }
    var programa = abrirProgramaDatafono();
    
    // Se verifica el archivo de respuesta que da el datafono una vez procese la compra
    var revision_respuesta = setInterval(
        function () {
            var filepath = "RESPUESTA.txt";
            fs.readFile(filepath, 'utf-8', function (err, data) {
                if(err){
                    console.log("An error ocurred reading the file :" + err.message);
                    return;
                }
                console.log("se leyo el archivo");
                content = data;
                separador = ",";
                arreglo = content.split(separador);
                
                var status = arreglo[0];
                var cod_aprob = arreglo[1];
                var num_tarjeta = arreglo[2];
                var cta = arreglo[3];
                var franquicia = arreglo[4];
                var monto = parseInt(arreglo[5]);
                var iva = parseInt(arreglo[6]);
                var base_dev = parseInt(arreglo[7]);
                var imp_consu = parseInt(arreglo[8]);
                var num_rec = arreglo[9];
                var cuota = arreglo[10];
                var rrn = arreglo[11];
                var terminal = arreglo[12];
                var cod_estab = arreglo[13];
                var fecha = arreglo[14];
                var hora = arreglo[15];
                var num_bono = arreglo[15];           
                if (status == '00') {
                    console.log("im here");
                    json_response = 
                    {
                        "status": "APPROVED",  
                        "cod_aprob": cod_aprob, 
                        "num_tarjeta": num_tarjeta,
                        "cta": cta, 
                        "franquicia": franquicia, 
                        "monto": monto,
                        "iva": iva,
                        "base_dev": base_dev, 
                        "imp_consu": imp_consu,
                        "num_rec": num_rec
                    }
                    
                } else if (status != '00') {
                    console.log("bad news :(");
                    json_response = 
                    {
                        "status": "REJECTED",  
                        "cod_aprob": cod_aprob, 
                        "num_tarjeta": num_tarjeta,
                        "cta": cta, 
                        "franquicia": franquicia, 
                        "monto": monto,
                        "iva": iva,
                        "base_dev": base_dev, 
                        "imp_consu": imp_consu,
                        "num_rec": num_rec
                    }
                }
                // Se retorna la respuest a la app y se cierra el .exe
                res.status(200).json(json_response);
                programa.kill();
                // Si es aprobada la peticion se envìa el correo electronico
                var date_totay = new Date().toISOString().slice(0,10);
                if (status == '00') {
                    var result = "APROBADA"
                var text_email =
                `<p style='text-align: justify;'><span style='font-size: 14pt; font-family: url('fonts/MyriadPro-Light.woff');'>
                <span style='font-size: 19pt; color: #000080;'>
                <img src='https://firebasestorage.googleapis.com/v0/b/visa-barra-libre.appspot.com/o/MAILING-NOTIFICACION-CARGA-HEADER.png?alt=media&token=61b23a0b-5fbe-4b9f-8c71-d18bbdcca223' alt=' width='600' height='100' /><br />
                Carga exitosa</span><br />
                <span style='font-size: 16px; font-family: url('fonts/MyriadPro-Light.woff');'><br />
                <p style='font-size: 16px;'>Apreciado Cliente.<br /><br/>
                Te informamos que has realizado exitosamente la carga de tu <br />
                vaso con el monto que elegiste, ahora disfruta con Martes Visa<br />
                sin excesos.</p></span>
                  <p style='text-align: justify;'><span style='font-size: 12pt;'><br />
                  <span style='color: #000080;'><strong><span style='font-size: 11pt;'>Resumen de la transacci&oacute;n:&nbsp;</span></strong></span><br />
                  <br />
                  <span style='color: #000080, font-family: url('fonts/MyriadPro-Light.woff');'><span style='color: #000080'>Valor&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ${monto}<br /></span>
                  <span style='color: #000080'>Fecha&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;${date_totay}<br /></span>
                  <span style='color: #000080'>N&uacute;mero de tarjeta&nbsp; &nbsp; &nbsp; &nbsp; ${num_tarjeta}<br /></span>
                  <span style='color: #000080'>Medio de pago&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ${franquicia}</span></span></p></span>
                  <p style='text-align: justify;'>&nbsp;</p>
                  <p style='text-align: justify;'>&nbsp;</p>
                  <p style='text-align: justify;'><span style='font-size: 12pt;'><span style='font-family: url('fonts/MyriadPro-Light.woff'), color: #000080;'><img src='https://firebasestorage.googleapis.com/v0/b/visa-barra-libre.appspot.com/o/MAILING-NOTIFICACION-CARGA-FOOTER.png?alt=media&token=9af76456-2354-4285-9e94-32b166712f2a' alt=' width='600' height='50' /></span></span></p>
                  <p style='text-align: justify;'>&nbsp;</p>
                  <p style='text-align: justify;'>&nbsp;</p>`
                    
                  var transport = nodemailer.createTransport({
                    host: 'smtp.gmail.com',
                    port: 465,
                    secure: true,
                    auth: {
                      user: 'info@barralibrevisa.com',
                      pass: 'Colombia2019!'
                    }
                    });
                    
                    var mailOptions={
                        from: 'info@barralibrevisa.com',
                        to: user_email,
                        subject: 'NOTIFICACIÓN DE COMPRA MARTES VISA/BARRA LIBRE',
                        text: "hola",
                        html: text_email
                    };
                
                      transport.sendMail(mailOptions, function(error, info){
                        if (error) {
                          console.log(error);
                        } else {
                          console.log('Email sent: ' + info.response);
                        }
                      });
                    }
                clearInterval(revision_respuesta);
            });
          
    }, 3000);


   
});

// De aqui para abajo son metòdos de pruebas

app.get('/send_email', function(req, res) {
    var today = new Date();
    var date_totay = new Date().toISOString().slice(0,10);
    var user_email = "cesar.torres@mocionsoft.com"
    var status = "00";
    var cod_aprob = 123456;
    var num_tarjeta = "4506******121314";
    var cta = 19274324;
    var franquicia = "VISA";
    var monto = 150000;
    var iva = 0;
    var base_dev = 0;
    var imp_consu = 0;
    var num_rec = 120641;
    var cuota = 1;
    var rrn = 109;
    var terminal = 1560; 
    var cod_estab = 123098;
    if (status == '00') {
        var text_email =
        ` <p style='text-align: justify;'><span style='font-size: 14pt; font-family: url('fonts/MyriadPro-Light.woff');'>
        <span style='font-size: 19pt; color: #000080;'>
        <img src='https://firebasestorage.googleapis.com/v0/b/visa-barra-libre.appspot.com/o/MAILING-NOTIFICACION-CARGA-HEADER.png?alt=media&token=61b23a0b-5fbe-4b9f-8c71-d18bbdcca223' alt=' width='600' height='100' /><br />
        Carga exitosa</span><br />
        <span style='font-size: 16px; font-family: url('fonts/MyriadPro-Light.woff');'><br />
        <p style='font-size: 16px;'>Apreciado Cliente.<br /><br/>
        Te informamos que has realizado exitosamente la carga de tu <br />
        vaso con el monto que elegiste, ahora disfruta con Martes Visa<br />
        sin excesos.</p></span>
          <p style='text-align: justify;'><span style='font-size: 12pt;'><br />
          <span style='color: #000080;'><strong><span style='font-size: 11pt;'>Resumen de la transacci&oacute;n:&nbsp;</span></strong></span><br />
          <br />
          <span style='color: #000080, font-family: url('fonts/MyriadPro-Light.woff');'><span style='color: #000080' border-bottom:solid;>Valor&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ${monto}<br /></span>
          <span style='color: #000080' border-bottom:solid;>Fecha&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;${date_totay}<br /></span>
          <span style='color: #000080' border-bottom:solid;>N&uacute;mero de tarjeta&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ${num_tarjeta}<br /></span>
          <span style='color: #000080' border-bottom:solid;>Medio de pago&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ${franquicia}</span></span></p></span>
          <p style='text-align: justify;'>&nbsp;</p>
          <p style='text-align: justify;'>&nbsp;</p>
          <p style='text-align: justify;'><span style='font-size: 12pt;'><span style='font-family: url('fonts/MyriadPro-Light.woff'), color: #000080;'><img src='https://firebasestorage.googleapis.com/v0/b/visa-barra-libre.appspot.com/o/MAILING-NOTIFICACION-CARGA-FOOTER.png?alt=media&token=9af76456-2354-4285-9e94-32b166712f2a' alt=' width='600' height='50' /></span></span></p>
          <p style='text-align: justify;'>&nbsp;</p>
          <p style='text-align: justify;'>&nbsp;</p>`
            
            var transport = nodemailer.createTransport({
                host: 'smtp.gmail.com',
                port: 465,
                secure: true,
                auth: {
                  user: 'info@barralibrevisa.com',
                  pass: 'Colombia2019!'
                }
                });
                
        
                var mailOptions={
                    from: 'info@barralibrevisa.com',
                    to: user_email,
                    subject: 'NOTIFICACIÓN DE COMPRA MARTES VISA/BARRA LIBRE',
                    text: "hola",
                    html: text_email
                };
        
        
              
              transport.sendMail(mailOptions, function(error, info){
                if (error) {
                  console.log(error);
                } else {
                  console.log('Email sent: ' + info.response);
                }
              });
    } 
   
});

app.get('/test_response', function(req, res) {
var filepath = "test_respuesta.txt";
            fs.readFile(filepath, 'utf-8', function (err, data) {
                if(err){
                    alert("An error ocurred reading the file :" + err.message);
                    return;
                }
                console.log("se leyo el archivo");
                content = data;
                separador = ",";
                arreglo = content.split(separador);
                
                var status = arreglo[0];
                var cod_aprob = arreglo[1];
                var num_tarjeta = arreglo[2];
                var cta = arreglo[3];
                var franquicia = arreglo[4];
                var monto = parseInt(arreglo[5]);
                var iva = parseInt(arreglo[6]);
                var base_dev = parseInt(arreglo[7]);
                var imp_consu = parseInt(arreglo[8]);
                var num_rec = arreglo[9];
                var cuota = arreglo[10];
                var rrn = arreglo[11];
                var terminal = arreglo[12];
                var cod_estab = arreglo[13];
                var fecha = arreglo[14];
                var hora = arreglo[15];
                var num_bono = arreglo[15];           
                if (status == '00') {
                    console.log("im here");
                    json_response = 
                    {
                        "status": "APPROVED",  
                        "cod_aprob": cod_aprob, 
                        "num_tarjeta": num_tarjeta,
                        "cta": cta, 
                        "franquicia": franquicia, 
                        "monto": monto,
                        "iva": iva,
                        "base_dev": base_dev, 
                        "imp_consu": imp_consu,
                        "num_rec": num_rec

                    }
                    
                } else if (status != '00') {
                    console.log("bad news :(");
                    json_response = 
                    {
                        "status": "REJECTED",  
                        "cod_aprob": cod_aprob, 
                        "num_tarjeta": num_tarjeta,
                        "cta": cta, 
                        "franquicia": franquicia, 
                        "monto": monto,
                        "iva": iva,
                        "base_dev": base_dev, 
                        "imp_consu": imp_consu,
                        "num_rec": num_rec

                    }
                }
                console.log(json_response);
                res.status(200).json(json_response);

            });

        });
        
        app.get('/test_response', function(req, res) {
                    <p style="text-align: justify;"><span style="font-size: 14pt; font-family: url(;"> <span style="font-size: 19pt; color: #000080;"> <img src="https://firebasestorage.googleapis.com/v0/b/visa-barra-libre.appspot.com/o/MAILING-NOTIFICACION-CARGA-HEADER.png?alt=media&amp;token=61b23a0b-5fbe-4b9f-8c71-d18bbdcca223" alt=" width=" height="100" /><br />Carga exitosa</span><br /><span style="font-size: 16px; font-family: url(;"><br /></span></span></p>
        <p style="font-size: 16px;">Apreciado Cliente.<br /><br />Te informamos que has realizado exitosamente tu compra en<br />la Barra Libre de Excusas Visa. Disfruta Martes Vida con un 30%<br />de dto.* en m&aacute;s de 200 restaurantes y sin reservar para<br />celebrar lo que quieras</p>
        <p style="text-align: justify;"><span style="font-size: 12pt;"><br /><span style="color: #000080;"><strong><span style="font-size: 11pt;">Resumen de la compra:&nbsp;</span></strong></span><br /><br /><span style="color: #000080, font-family: url(;"><span style="color: #000080;">Valor&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ${monto}<br /></span> <span style="color: #000080;">Fecha&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;${date_totay}<br /></span> <span style="color: #000080;">N&uacute;mero de tarjeta&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ${num_tarjeta}<br /></span> <span style="color: #000080;">Medio de pago&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ${franquicia}</span></span></span></p>
        <hr style="color: #000080;" align="left" width="61%" />
        <p style="text-align: justify;">&nbsp;</p>
        <p style="text-align: justify;">&nbsp;</p>
        <p style="text-align: justify;"><span style="font-size: 12pt;"><span style="font-family: url(;"><img src="https://firebasestorage.googleapis.com/v0/b/visa-barra-libre.appspot.com/o/MAILING-NOTIFICACION-CARGA-FOOTER.png?alt=media&amp;token=9af76456-2354-4285-9e94-32b166712f2a" alt=" width=" height="50" /></span></span></p>
        <p style="text-align: justify;">&nbsp;</p>
        <p style="text-align: justify;">&nbsp;</p>
                });
        app.listen(8010);


console.log("Servidor escuchando por el puerto :  8010");